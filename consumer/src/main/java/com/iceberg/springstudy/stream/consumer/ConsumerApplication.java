package com.iceberg.springstudy.stream.consumer;

import com.iceberg.springstudy.stream.common.event.CustomApplicationEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.bus.jackson.RemoteApplicationEventScan;

@Slf4j
@SpringBootApplication
@RemoteApplicationEventScan(basePackageClasses = CustomApplicationEvent.class)
public class ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }
}
